import React, { useEffect, useState } from "react";
import { Table, Button } from "reactstrap";
import styled from 'styled-components';


const ClicableTh = styled.th`
    cursor: pointer;
    :hover {
        text-decoration: underline;
        color: red;
    }
`;

const Weather = (props) => {
    const nombreCol = "model";
    const [weatherDataa, setweatherDataa]  = useState([]);

    useEffect(()=>{
        fetch("https://api.openweathermap.org/data/2.5/box/city?bbox=12,32,15,37,10&appid=16b2c28ee3a3097b61466e9f6f8c5e08")
        .then(data => data.json())
        .then(weatherData => console.log(weatherData.list))
        .catch(error => console.log(error));
    
        
    },[])

    
    {const weatherApi = weatherDataa.map((value) =>{
    //   console.log(value);
        console.log(value);
    })}

// console.log(props.datos);

    const filas =props.datos.map((el) => (
      <tr key={el.id}>
        <td>{el.id}</td>
        <td>{el.name}</td>
        <td>{el.main && Math.round(el.main.temp) + "°C"}</td>
        <td><img src={`http://openweathermap.org/img/w/${el.weather[0].icon}.png`}/></td>
    

        <td>
          <Button color="danger" onClick={() => props.borra(el.id)}>
            Borrar
          </Button>
          {" "}
          <Button color="success" onClick={() => props.muestra(el.id)}>
            Mostrar
          </Button>
        </td>
      </tr>
    ));
  
    return (
      <Table striped>
        <thead>
          <tr>
            <ClicableTh onClick={() => props.ordena('id', true)}>#</ClicableTh>
            <ClicableTh onClick={() => props.ordena('marca')}>Name</ClicableTh>
            <ClicableTh onClick={() => props.ordena('model')}>Temperature</ClicableTh>
            <ClicableTh onClick={() => props.ordena('model')}>Weather</ClicableTh>

            <th></th>
          </tr>
          <tr>
            <td></td>
            <td>
              <input onChange={(e) => props.filtra(e.target.value, "marca")} />
            </td>
            <td>
              <input onChange={(e) => props.filtra(e.target.value, "model")} />
            </td>
            <td></td>
          </tr>
        </thead>
        <tbody>{filas}</tbody>
        <tfoot></tfoot>
      </Table>
    );
  };
  
export default Weather;